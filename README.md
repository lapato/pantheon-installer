# Pantheon Desktop for Arch Linux 

A easy way to install Pantheon Desktop on Arch Linux

[elementary OS](https://elementary.io/) releases are derived from [Ubuntu's](https://wiki.archlinux.org/index.php/Arch_compared_to_other_distributions#Ubuntu) LTS releases, typically trailing Ubuntu's cycle by a few weeks or  months. However, its constituent packages are updated continuously. See  the official [github repository](https://github.com/elementary) and consult their [community slack](https://elementarycommunity.slack.com/).


![scren_1](images/desktop_arch.png)

![scren_2](images/neofetch_arch.png)


## Installation 
1. Clone Repository 
```
git clone https://gitlab.com/farhansadik/pantheon-installer.git 
```
2. Run Installer <br>
> Do not run as **root**. 
```
./install
```
### Notes 
* If you've seen any dependency missing then install it manually. 
* When this `pantheon-dock-git and plank are in conflict. Remove plank? [y/N] ` message shows, then just press **y**. 

## Uninstall 
You can uninstall pantheon but executing `uninstall.bash` file. But this script is not recommended. It could damage your system. It would be better to uninstall those packages manually.

## List of Aur Packages
* [pantheon-dock-git](https://aur.archlinux.org/packages/pantheon-dock-git)
* [switchboard-plug-pantheon-tweaks-git](https://aur.archlinux.org/packages/switchboard-plug-pantheon-tweaks-git)

## Tweaks

 * **Change terminal font**

	Open `dconf-editor`. Go to `io.elementary.terminal.settings.font`. Enter your preffered font name on '**custom value**' option. Just enter as `'DejaVu Sans Mono Book 12'` without quotation.
	```
	Font Name : DejaVu Sans Mono Book
	Font Size : 12 
	```
## Theme and Icon 
* [Theme](https://github.com/vinceliuice/WhiteSur-gtk-theme)
* [Icon](https://github.com/vinceliuice/WhiteSur-icon-theme)
* [Grub Theme](https://github.com/vinceliuice/grub2-themes )
* [Cursor](https://www.gnome-look.org/p/1411743)

## Sources and Credits
1. [Arch Wiki](https://wiki.archlinux.org/title/Pantheon)
2. [Pantheon Group](https://archlinux.org/groups/x86_64/pantheon/) 
3. [https://www.debugpoint.com/pantheon-arch-linux-install/](https://www.debugpoint.com/pantheon-arch-linux-install/)



## Farhan Sadik
