#!/bin/bash 

# checked v4.1 

################################################################################
#
#	THIS UNINSTALL SCRIPT CAN BE HARMFUL FOR YOUR DEVIE
# 	IT WOULD BE BETTER IF YOU REMOVE PACKAGES MANUALLY 
#
################################################################################

# conflict [1]
sudo pacman -Rcns elementary-icon-theme xfce4-settings
sudo pacman -Rcns pantheon-session wingpanel

# aur packages 
sudo pacman -Rcns pantheon-dock-git  switchboard-plug-pantheon-tweaks-git

# dependencies 
sudo pacman -Rcns cerbere contractor file-roller gala geary onboard orca simple-scan  

# group 
sudo pacman -Rcns pantheon

# reinstalling conflict pckages 
sudo pacman -S elementary-icon-theme xfce4-settings gnome-themes-extra python 


# checking orphans 
pacman -Qtdq

# cleaning orphans 
sudo pacman -Rns $(pacman -Qtdq)
